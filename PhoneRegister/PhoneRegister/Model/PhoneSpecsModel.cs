﻿using PhoneRegister.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneRegister.Model
{
    class PhoneSpecsModel
    {
        private static List<PhoneSpecs> lstPhone = new List<PhoneSpecs>();


        public static List<PhoneSpecs> getlist()
        {
            return lstPhone;
        }

        public static void pupulate()
        {
            PhoneSpecs[] phoneSpecs =
            {
                new PhoneSpecs("samsung a10","Samsung", "available","6.02 inches", "180g", "plastic", "dual", "lcd","6 inches",
                "720p", "glass", "Andriod Pie", "exynos", "octacore", "mali", "32gb", "yes", 30)
            };

            lstPhone = phoneSpecs.ToList();
        }
    }
}
