﻿using PhoneRegister.entities;
using PhoneRegister.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneRegister.GUI
{
    public partial class FrmMain : Form
    {
        private DataTable dtSistema;
     
        public FrmMain()
        {
            InitializeComponent();
        }

        private void registroCelularesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistroCelulares frc = new FrmRegistroCelulares();
            frc.MdiParent = this;
            frc.DsSistema = dsSistema;
            frc.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtSistema = dsSistema.Tables["PhoneSpecs"];
            PhoneSpecsModel.pupulate();

            foreach(PhoneSpecs p in PhoneSpecsModel.getlist())
            {
                dtSistema.Rows.Add(p.Name, p.Brand, p.Status, p.Dimension, p.Weigth, p.Build, p.Sim, 
                    p.DisplayType, p.DisplaySize, p.DisplayResolution, p.DisplayProteccion, p.Os, 
                    p.Chipset, p.Cpu, p.Gpu,p.Internalmemory, p.CardSlot, p.Cantidad1);

            } 
        }
    }
}
