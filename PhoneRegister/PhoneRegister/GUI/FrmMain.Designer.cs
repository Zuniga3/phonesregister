﻿namespace PhoneRegister.GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.registroCelularesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dsSistema = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.bsSistema = new System.Windows.Forms.BindingSource(this.components);
            this.dataColumn18 = new System.Data.DataColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsSistema)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSistema)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registroCelularesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(632, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // registroCelularesToolStripMenuItem
            // 
            this.registroCelularesToolStripMenuItem.Name = "registroCelularesToolStripMenuItem";
            this.registroCelularesToolStripMenuItem.Size = new System.Drawing.Size(124, 20);
            this.registroCelularesToolStripMenuItem.Text = "Gestion de celulares";
            this.registroCelularesToolStripMenuItem.Click += new System.EventHandler(this.registroCelularesToolStripMenuItem_Click);
            // 
            // dsSistema
            // 
            this.dsSistema.DataSetName = "NewDataSet";
            this.dsSistema.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18});
            this.dataTable1.TableName = "PhoneSpecs";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Name";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Brand";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Status";
            this.dataColumn3.ColumnName = "Status";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Dimension";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Weigth";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "build";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "SIM";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "DisplayType";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "DisplaySize";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "DisplayResolution ";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "DisplayProtection";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "OS";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "chipset";
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "CPU";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "GPU";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "InternalMemory";
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "CardSlot";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "Cantidad";
            this.dataColumn18.DataType = typeof(int);
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(632, 426);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Text = "Registro de celulares";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsSistema)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSistema)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem registroCelularesToolStripMenuItem;
        private System.Data.DataSet dsSistema;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Windows.Forms.BindingSource bsSistema;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
    }
}