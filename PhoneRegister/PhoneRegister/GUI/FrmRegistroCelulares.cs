﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneRegister.GUI
{
    public partial class FrmRegistroCelulares : Form
    {
        private DataSet dsSistema;
        private BindingSource bsSistema;

        public DataSet DsSistema
        {
            get
            {
                return dsSistema;
            }

            set
            {
                dsSistema = value;
            }
        }

        public FrmRegistroCelulares()
        {
            InitializeComponent();
            bsSistema = new BindingSource();
        }

        private void txtfinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsSistema.Filter = string.Format("Name like '*{0}*' or Brand like '*{0}*' or Status like '*{0}*'", txtfinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmRegistroCelulares_Load(object sender, EventArgs e)
        {
            bsSistema.DataSource = DsSistema;
            bsSistema.DataMember = DsSistema.Tables["PhoneSpecs"].TableName;
            dgvPhones.DataSource = bsSistema;
            dgvPhones.AutoGenerateColumns = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmPhone fp = new FrmPhone();
            fp.Show();
        }
    }
}
