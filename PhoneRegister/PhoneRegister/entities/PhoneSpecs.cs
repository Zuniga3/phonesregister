﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneRegister.entities
{
    class PhoneSpecs
    {
        private string name;
        private string brand;
        private string status;
        private string dimension;
        private string weigth;
        private string build;
        private string sim;
        private string displayType;
        private string displaySize;
        private string displayResolution;
        private string displayProteccion;
        private string os;
        private string chipset;
        private string cpu;
        private string gpu;
        private string internalmemory;
        private string cardSlot;
        private int Cantidad;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Brand
        {
            get
            {
                return brand;
            }

            set
            {
                brand = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public string Dimension
        {
            get
            {
                return dimension;
            }

            set
            {
                dimension = value;
            }
        }

        public string Weigth
        {
            get
            {
                return weigth;
            }

            set
            {
                weigth = value;
            }
        }

        public string Build
        {
            get
            {
                return build;
            }

            set
            {
                build = value;
            }
        }

        public string Sim
        {
            get
            {
                return sim;
            }

            set
            {
                sim = value;
            }
        }

        public string DisplayType
        {
            get
            {
                return displayType;
            }

            set
            {
                displayType = value;
            }
        }

        public string DisplaySize
        {
            get
            {
                return displaySize;
            }

            set
            {
                displaySize = value;
            }
        }

        public string DisplayResolution
        {
            get
            {
                return displayResolution;
            }

            set
            {
                displayResolution = value;
            }
        }

        public string DisplayProteccion
        {
            get
            {
                return displayProteccion;
            }

            set
            {
                displayProteccion = value;
            }
        }

        public string Os
        {
            get
            {
                return os;
            }

            set
            {
                os = value;
            }
        }

        public string Chipset
        {
            get
            {
                return chipset;
            }

            set
            {
                chipset = value;
            }
        }

        public string Cpu
        {
            get
            {
                return cpu;
            }

            set
            {
                cpu = value;
            }
        }

        public string Gpu
        {
            get
            {
                return gpu;
            }

            set
            {
                gpu = value;
            }
        }

        public string Internalmemory
        {
            get
            {
                return internalmemory;
            }

            set
            {
                internalmemory = value;
            }
        }

        public string CardSlot
        {
            get
            {
                return cardSlot;
            }

            set
            {
                cardSlot = value;
            }
        }

        public int Cantidad1
        {
            get
            {
                return Cantidad;
            }

            set
            {
                Cantidad = value;
            }
        }

        private PhoneSpecs()
        {

        }

        public PhoneSpecs(string name, string brand, string status, string dimension, string weigth, string build, string sim, string displayType, string displaySize, string displayResolution, string displayProteccion, string os, string chipset, string cpu, string gpu, string internalmemory, string cardSlot)
        {
            this.Name = name;
            this.Brand = brand;
            this.Status = status;
            this.Dimension = dimension;
            this.Weigth = weigth;
            this.Build = build;
            this.Sim = sim;
            this.DisplayType = displayType;
            this.DisplaySize = displaySize;
            this.DisplayResolution = displayResolution;
            this.DisplayProteccion = displayProteccion;
            this.Os = os;
            this.Chipset = chipset;
            this.Cpu = cpu;
            this.Gpu = gpu;
            this.Internalmemory = internalmemory;
            this.CardSlot = cardSlot;
        }

        public PhoneSpecs(string name, string brand, string status, string dimension, string weigth, string build, string sim, string displayType, string displaySize, string displayResolution, string displayProteccion, string os, string chipset, string cpu, string gpu, string internalmemory, string cardSlot, int cantidad)
        {
            this.name = name;
            this.brand = brand;
            this.status = status;
            this.dimension = dimension;
            this.weigth = weigth;
            this.build = build;
            this.sim = sim;
            this.displayType = displayType;
            this.displaySize = displaySize;
            this.displayResolution = displayResolution;
            this.displayProteccion = displayProteccion;
            this.os = os;
            this.chipset = chipset;
            this.cpu = cpu;
            this.gpu = gpu;
            this.internalmemory = internalmemory;
            this.cardSlot = cardSlot;
            Cantidad1 = cantidad;
        }
    }
}
